package com.gildedrose;

public interface ItemStrategy {
    boolean applies(Item item);

    void apply(Item item);
}