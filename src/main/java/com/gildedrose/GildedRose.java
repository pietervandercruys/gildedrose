package com.gildedrose;

import com.gildedrose.strategies.*;

import java.util.Arrays;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    DefaultStrategy defaultStrategy = new DefaultStrategy();
    private final ItemStrategy[] strategies = new ItemStrategy[]{
            new AgedBrieStrategy(),
            new SulfurasStrategy(),
            new BackstagePassesStrategy(),
            new ConjuredStrategy()
    };

    public void updateQuality() {
        Arrays.stream(items).forEach(this::updateQuality);
    }

    private void updateQuality(Item item) {
        Arrays.stream(strategies)
                .filter(strategy -> strategy.applies(item))
                .findAny()
                .orElse(defaultStrategy).apply(item);
    }
}