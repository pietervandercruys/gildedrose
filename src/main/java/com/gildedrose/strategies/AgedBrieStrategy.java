package com.gildedrose.strategies;

import com.gildedrose.Item;
import com.gildedrose.ItemStrategy;

public class AgedBrieStrategy implements ItemStrategy {
    private StrategyUtils utils = new StrategyUtils();

    @Override
    public boolean applies(Item item) {
                return "Aged Brie".equals(item.name);
    }

    @Override
    public void apply(Item item) {
        utils.updateQuality(item, utils.isExpired(item) ? 2 : 1);
        utils.decreaseSellIn(item);
    }
}
