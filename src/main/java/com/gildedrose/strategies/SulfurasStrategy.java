package com.gildedrose.strategies;

import com.gildedrose.Item;
import com.gildedrose.ItemStrategy;

public class SulfurasStrategy implements ItemStrategy {
    @Override
    public boolean applies(Item item) {
        return "Sulfuras, Hand of Ragnaros".equals(item.name);
    }

    @Override
    public void apply(Item item) {
    }
}
