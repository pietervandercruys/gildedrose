package com.gildedrose.strategies;

import com.gildedrose.Item;
import com.gildedrose.ItemStrategy;

public class DefaultStrategy implements ItemStrategy {
    private StrategyUtils utils = new StrategyUtils();

    @Override
    public boolean applies(Item item) {
        throw new RuntimeException("No implementation, this is the default, use it as a default");
    }

    @Override
    public void apply(Item item) {
        utils.updateQuality(item, utils.isExpired(item) ? -2 : -1);
        utils.decreaseSellIn(item);
    }
}
