package com.gildedrose.strategies;

import com.gildedrose.Item;
import com.gildedrose.ItemStrategy;

public class BackstagePassesStrategy implements ItemStrategy {
    private StrategyUtils utils = new StrategyUtils();

    @Override
    public boolean applies(Item item) {
        return "Backstage passes to a TAFKAL80ETC concert".equals(item.name);
    }

    @Override
    public void apply(Item item) {
        int qualityAmount = 1;
        if(utils.isExpired(item)) {
            qualityAmount = -item.quality;
        }
        else if(utils.expiresIn(item, 5)) {
            qualityAmount = 3;
        }
        else if(utils.expiresIn(item, 10)) {
            qualityAmount = 2;
        }
        utils.updateQuality(item, qualityAmount);
        utils.decreaseSellIn(item);
    }
}
