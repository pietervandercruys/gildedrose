package com.gildedrose.strategies;

import com.gildedrose.Item;
import com.gildedrose.ItemStrategy;

public class ConjuredStrategy implements ItemStrategy {
    private StrategyUtils utils = new StrategyUtils();

    @Override
    public boolean applies(Item item) {
        return "Conjured Mana Cake".equals(item.name);
    }

    @Override
    public void apply(Item item) {
        utils.updateQuality(item, utils.isExpired(item) ? -4 : -2);
        utils.decreaseSellIn(item);
    }
}
