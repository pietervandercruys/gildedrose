package com.gildedrose.strategies;

import com.gildedrose.Item;

public class StrategyUtils {

    public void updateQuality(Item item, int amount) {
        int newValue = item.quality + amount;
        newValue = Math.max(newValue, 0);
        newValue = Math.min(newValue, 50);
        item.quality = newValue;
    }

    public void decreaseSellIn(Item item) {
        item.sellIn = item.sellIn - 1;
    }

    public boolean isExpired(Item item) {
        return expiresIn(item, 0);
    }

    public boolean expiresIn(Item item, int days) {
        return item.sellIn <= days;
    }
}
