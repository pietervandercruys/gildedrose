package com.gildedrose;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TexttestFixture {
    public static void main(String[] args) throws IOException {
        System.out.println("OMGHAI!");

        Item[] items = new Item[] {
                new Item("+5 Dexterity Vest", 10, 20), //
                new Item("Aged Brie", 2, 0), //
                new Item("Elixir of the Mongoose", 5, 7), //
                new Item("Sulfuras, Hand of Ragnaros", 0, 80), //
                new Item("Sulfuras, Hand of Ragnaros", -1, 80),
                new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
                new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
                new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
                // this conjured item does not work properly yet
                new Item("Conjured Mana Cake", 3, 6) };

        GildedRose app = new GildedRose(items);

        int days = 100;
        if (args.length > 0) {
            days = Integer.parseInt(args[0]) + 1;
        }
        String itemsAsText = "";
        for (int i = 0; i < days; i++) {
            itemsAsText += "-------- day " + i + " --------\n";
            itemsAsText += "name, sellIn, quality\n";
            for (Item item : items) {
                itemsAsText += item.toString()+'\n';
            }
            itemsAsText += '\n';
            app.updateQuality();
        }
        System.out.println(itemsAsText);
        FileOutputStream fos = new FileOutputStream("gilded-rose-test.txt");
        fos.write(itemsAsText.getBytes());
        fos.close();


        String goldenMaster = new String(Files.readAllBytes(Paths.get("golden-master.txt")));
        assertEquals(goldenMaster, itemsAsText);
    }

}
