package com.gildedrose.strategies;

import com.gildedrose.Item;
import com.gildedrose.ItemStrategy;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

public class ConjuredStrategyTest {
    private ItemStrategy strategy = new ConjuredStrategy();

    @Test
    void itAppliesConjuredItems() {
        assertTrue(strategy.applies(new Item("Conjured Mana Cake", 0, 0)));
    }

    @Test
    void itDoesNotApplyOtherItems() {
        assertFalse(strategy.applies(new Item("Mana Cake", 0, 0)));
    }

    @Test
    void itReducesSellsInBy1() {
        Item item = new Item("Conjured Mana Cake", 3, 0);
        strategy.apply(item);
        assertEquals(item.sellIn, 2);
        strategy.apply(item);
        assertEquals(item.sellIn, 1);
        strategy.apply(item);
        assertEquals(item.sellIn, 0);
        strategy.apply(item);
        assertEquals(item.sellIn, -1);
        strategy.apply(item);
        assertEquals(item.sellIn, -2);
    }

    @Test
    void itReducesQualityBy2WhenItemIsNotExpired() {
        Item item = new Item("Conjured Mana Cake", 100, 10);
        strategy.apply(item);
        assertEquals(item.quality, 8);
        strategy.apply(item);
        assertEquals(item.quality, 6);
        strategy.apply(item);
        assertEquals(item.quality, 4);
        strategy.apply(item);
        assertEquals(item.quality, 2);
        strategy.apply(item);
        assertEquals(item.quality, 0);
    }

    @Test
    void itReducesQualityBy4WhenItemIsExpired() {
        Item item = new Item("Conjured Mana Cake", 0, 20);
        strategy.apply(item);
        assertEquals(item.quality, 16);
        strategy.apply(item);
        assertEquals(item.quality, 12);
        strategy.apply(item);
        assertEquals(item.quality, 8);
        strategy.apply(item);
        assertEquals(item.quality, 4);
        strategy.apply(item);
        assertEquals(item.quality, 0);
    }

    @Test
    void qualityIsNeverLowerThen0() {
        Item item = new Item("Conjured Mana Cake", 0, 20);
        IntStream.range(0, 9999).forEach(i -> strategy.apply(item));
        assertEquals(item.quality, 0);
    }
}
